#include <iostream>
#include <queue>
#include <time.h>
#include <stdlib.h>

using namespace std;

template <typename T>
void print(T value){
	while(!value.empty()){
		cout << value.top() << " ";
		value.pop();
	}
	cout << endl;
}

template <typename T>
bool compare(T x, T y){
	return x > y;
}

template <typename T>
bool isSingle(T x, T y){
	return x % 2;
}
// priority queue default olarak buyukten kucuge siralayarak ekler
int main(){
	srand(time(0));
	vector <int> vec;

	{
		cout << "**** Default Priority Queue Sayilara Gore ****" << endl;
		priority_queue<int> p1;
		p1.push(4);
		print(p1);
		p1.push(17);
		print(p1);
		p1.push(10);
		print(p1);
		p1.push(3);
		print(p1);
		p1.push(5);
		print(p1);
		p1.push(17);
		print(p1);
	}

	{
	cout << endl;
	cout << "**** Kucukten Buyuge Sayilara Gore ****" << endl;
	
	priority_queue<int, vector<int>, bool(*)(int, int)> p3(compare);
	p3.push(13);
	print(p3);
	p3.push(1);
	print(p3);
	p3.push(19);
	print(p3);
	p3.push(14);
	print(p3);
	p3.push(8);
	print(p3);
	p3.push(5);
	print(p3);
	}
		
	{
	cout << endl;
	cout << "**** Rastgele Sayilara Gore ****" << endl;
	int randomNumber;
	for (int i; i < 10; i++){
		randomNumber = rand() % 100;
		cout << randomNumber << " ";
		vec.push_back(randomNumber);
	}
	cout << endl;
	priority_queue<int> p2(vec.begin(), vec.end());
	print(p2);
	}
		
	{		
	cout << endl;
	cout << "**** Rastgele Sayilarin Kucukten Buyuge Sayilara Gore ****" << endl;
	
	priority_queue<int, vector<int>, bool(*)(int, int)> p4(vec.begin(), vec.end(), compare);
	print(p4);
	}

	{
		cout << endl;
		cout << "**** Greater Sayilara Gore ****" << endl;
	    priority_queue<int, vector<int>, greater<int> > p5;
		p5.push(8);
		p5.push(7);
		p5.push(10);
		p5.push(14);
		p5.push(3);
		print(p5);
	}
}
