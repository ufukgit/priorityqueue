#include <iostream>
#include <queue>
#include <list>
using namespace std;

class Human{
	protected:
		string name;
		int age;
		bool isDisabledPerson;
	public:
		Human(string name, int age, bool isDisabledPerson=false) :
			name(name), age(age), isDisabledPerson(isDisabledPerson) {
			}
		
		void print()const{
		cout << " Name : "<< name << endl
			 << " Age : "<< age << endl
			 << " Disabled Person : ";
			 
			if (isDisabledPerson)
			 	cout << "Yes" ;
			else
				cout << "No";
			cout << endl;
		}
};

class Diseased : public Human{
	int diseasedNo;
	int priorityOrder;
	public:
		Diseased(int diseasedNo, Human& human) : diseasedNo(diseasedNo), Human(human), priorityOrder(0){
			calculatePriorityOrder();
		}
		
		Diseased(int diseasedNo, string name, int age, bool isDisabledPerson=false) :
			diseasedNo(diseasedNo), Human(name, age, isDisabledPerson), priorityOrder(0){
				calculatePriorityOrder();
			}
			
			void calculatePriorityOrder() {
				if (isDisabledPerson)
					priorityOrder += 20;
				if (age > 50)
					priorityOrder += age - 50;
			}
			
			bool operator <(const Diseased& diseased)const{
				return priorityOrder < diseased.priorityOrder;
			}
			
			void print()const{
			cout << " Diseased No : " << diseasedNo << endl;
			Human::print();
			cout << " Priority Order : " << priorityOrder << endl;
			}
};

class DiseasedAccept{
	list<Diseased> acceptList;
	priority_queue<Diseased> inspectionOrder;
	public:
		void acceptance(Human& human){
			Diseased dis1(acceptList.size(), human);
			acceptList.push_back(dis1);
			inspectionOrder.push(dis1);
		}
		
		void printAcceptOrder(){
			cout << " ### Diseased Acceptance Order ### "<< endl;
			if (acceptList.empty())
				cout << "No Diseased Acceptance" << endl;
			else{
				list<Diseased>::iterator iter = acceptList.begin();
				while(iter != acceptList.end()){
					Diseased dis1 = *iter;
					dis1.print();
					iter++;
					if (iter != acceptList.end())
						cout << endl;
				}
			}
			cout << " ### End Of Diseased Acceptance Order ###" << endl;	
		}
		
		void printInspectorOrder(){
			cout << " ### Diseased Inspection Order ### "<< endl;
			if (inspectionOrder.empty())
				cout << "No Diseased Inspector" << endl;
			else{
				cout << " Diseased Informations For Inspection" << endl;
				priority_queue<Diseased> queue = inspectionOrder;
				while(!queue.empty()){
					Diseased dis1 = queue.top();
					queue.pop();
					dis1.print();
					if (!queue.empty())
						cout << endl;	
				}
			}			
		}
		
		void callInspector(){
			cout << " ### CALL INSPECTOR! ### "<< endl;
			if (inspectionOrder.empty())
				cout << "No Inspector For Inspection" << endl;
			else{
				Diseased dis = inspectionOrder.top();
				dis.print();
				inspectionOrder.pop();
			}
		}
};

int main(){
	
	{
		/*	cout << "*** Print From Human ***" << endl;
		Human human1("Ufuk", 24);
		human1.print();
		cout << endl;
		Human human2("Ali", 44, 1);
		human2.print();
		cout << endl;
		Human human3("Ceren", 58);
		human3.print();
		cout << endl;
		Human human4("Gizem", 52, 1);
		human4.print();
		cout << endl;	
		cout << "*** Print From Diseased ***" << endl;	
		Diseased dis1(1, "Mustafa", 41);
		dis1.print();
		cout << endl;
		Diseased dis2(2, "Banu", 55, 1);
		dis2.print();
		cout << endl;	
		cout << "*** Print From Human To Diseased ***" << endl;
		Diseased dis3(3, human3);
		dis3.print();
		cout << endl;
		Diseased dis4(4, human4);
		dis4.print();
		cout << endl;	
		Human human1("Ufuk", 24);
		Human human2("Ali", 44, 1);
		Human human3("Ceren", 58);
		DiseasedAccept disAccept1;
		disAccept1.acceptance(human1);
		disAccept1.acceptance(human2);
		disAccept1.acceptance(human3);
		disAccept1.printAcceptOrder();*/	
	}

	Human people [] = {
		Human ("Ufuk", 24),
		Human ("Ali", 44, 1),
		Human ("Ceren", 78),
		Human ("Gizem", 62, 1),
		Human ("Mustafa", 51),
		Human ("Cansu", 55, 1),
		Human ("Banu", 85)
	};
	
	DiseasedAccept disAccept;
	for (int i = 0; i < 7; i++)
	{
		disAccept.acceptance(people[i]);
	}
	disAccept.printAcceptOrder();
	cout << endl;
	disAccept.printInspectorOrder();
	
	
	for (int i = 0; i < 7; i++)
	{
		disAccept.callInspector();
	}
	
	disAccept.callInspector();
} 
